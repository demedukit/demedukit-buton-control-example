################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Drivers/DemsayLib/Src/DigitalInputOutputs.c 

OBJS += \
./Drivers/DemsayLib/Src/DigitalInputOutputs.o 

C_DEPS += \
./Drivers/DemsayLib/Src/DigitalInputOutputs.d 


# Each subdirectory must supply rules for building sources it contributes
Drivers/DemsayLib/Src/%.o: ../Drivers/DemsayLib/Src/%.c Drivers/DemsayLib/Src/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m0plus -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32G030xx -c -I../Core/Inc -I"C:/Users/Onur/STM32CubeIDE/workspace_1.8.0/DEMEduKit/Drivers/DemsayLib/Inc" -I../Drivers/STM32G0xx_HAL_Driver/Inc -I../Drivers/STM32G0xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32G0xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

clean: clean-Drivers-2f-DemsayLib-2f-Src

clean-Drivers-2f-DemsayLib-2f-Src:
	-$(RM) ./Drivers/DemsayLib/Src/DigitalInputOutputs.d ./Drivers/DemsayLib/Src/DigitalInputOutputs.o

.PHONY: clean-Drivers-2f-DemsayLib-2f-Src

